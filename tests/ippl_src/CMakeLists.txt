add_subdirectory (AppTypes)
add_subdirectory (Field)
add_subdirectory (Index)
add_subdirectory (Meshes)
add_subdirectory (Particle)

set (TEST_SRCS_LOCAL ${TEST_SRCS_LOCAL} PARENT_SCOPE)
